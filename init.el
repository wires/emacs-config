;; emacs kicker --- kick start emacs setup
;; Copyright (C) 2010 Dimitri Fontaine
;;
;; Author: Dimitri Fontaine <dim@tapoueh.org>
;; URL: https://github.com/dimitri/emacs-kicker
;; Created: 2011-04-15
;; Keywords: emacs setup el-get kick-start starter-kit
;; Licence: WTFPL, grab your copy here: http://sam.zoy.org/wtfpl/
;;
;; This file is NOT part of GNU Emacs.

(require 'cl)				; common lisp goodies, loop

(add-to-list 'load-path "~/.emacs.d/el-get/el-get")

(unless (require 'el-get nil t)
  (url-retrieve
   "https://github.com/dimitri/el-get/raw/master/el-get-install.el"
   (lambda (s)
     (let
       (el-get-master-branch)
       (end-of-buffer)
       (eval-print-last-sexp)))))

;; now either el-get is `require'd already, or have been `load'ed by the
;; el-get installer.

;; set local recipes
(setq
 el-get-sources
 '((:name buffer-move			; have to add your own keys
	  :after (lambda ()
		   (global-set-key (kbd "<C-S-up>")     'buf-move-up)
		   (global-set-key (kbd "<C-S-down>")   'buf-move-down)
		   (global-set-key (kbd "<C-S-left>")   'buf-move-left)
		   (global-set-key (kbd "<C-S-right>")  'buf-move-right)))

   (:name smex				; a better (ido like) M-x
	  :after (lambda ()
		   (setq smex-save-file "~/.emacs.d/.smex-items")
		   (global-set-key (kbd "M-x") 'smex)
		   (global-set-key (kbd "M-X") 'smex-major-mode-commands)))

   (:name magit				; git meet emacs, and a binding
	  :after (lambda ()
		   (global-set-key (kbd "C-x C-z") 'magit-status)))
   (:name ProofGeneral-4.2 ;; Requires Emacs >= 23.3
       :type http-tar
       :options ("xzf")
       :url "http://proofgeneral.inf.ed.ac.uk/releases/ProofGeneral-4.1.tgz"
       :build ("cd ProofGeneral && make clean" "cd ProofGeneral && make compile")
       :load  ("ProofGeneral/generic/proof-site.el")
       :info "./ProofGeneral/doc/")
    (:name goto-last-change		; move pointer back to last change
	  :after (lambda ()
		   ;; when using AZERTY keyboard, consider C-x C-_
		   (global-set-key (kbd "C-x C-/") 'goto-last-change)))))

;; now set our own packages
(setq
 my:el-get-packages
 '(el-get				; el-get is self-hosting
;   escreen            			; screen for emacs, C-\ C-h
;   php-mode-improved			; if you're into php...
   switch-window			; takes over C-x o
   auto-complete			; complete as you type with overlays
;   zencoding-mode			; http://www.emacswiki.org/emacs/ZenCoding
   color-theme		                ; nice looking emacs
   color-theme-tango	                ; check out color-theme-solarized
   gist
   haskell-mode
;   yasnippet
   auto-complete-css
   auto-complete-yasnippet
   rainbow-delimiters
   nxhtml
   org-mode
   markdown-mode
   ProofGeneral-4.2
   deft
   color-theme-almost-monokai
   color-theme-ir-black
   auctex
   mmm-mode
))

(add-hook 'LaTeX-mode-hook (lambda ()
  (push 
    '("Latexmk" "latexmk -pdf %s" TeX-run-TeX nil t
      :help "Run Latexmk on file")
    TeX-command-list)))



;; Some recipes require extra tools to be installed
;;
;; Note: el-get-install requires git, so we know we have at least that.
;;
;;(when (el-get-executable-find "cvs")
;;  (add-to-list 'my:el-get-packages 'emacs-goodies-el)) ; the debian addons for emacs

;;(when (el-get-executable-find "svn")
;;  (loop for p in '(psvn    		; M-x svn-status
;;		   yasnippet		; powerful snippet mode
;;		   )
;;	do (add-to-list 'my:el-get-packages p)))

(setq my:el-get-packages
      (append
       my:el-get-packages
       (loop for src in el-get-sources collect (el-get-source-name src))))

;; install new packages and init already installed packages
(el-get 'sync my:el-get-packages)

;; on to the visual settings
(setq inhibit-splash-screen t)		; no splash screen, thanks
(line-number-mode 1)			; have line numbers and
(column-number-mode 1)			; column numbers in the mode line

(tool-bar-mode -1)			; no tool bar with icons
(scroll-bar-mode -1)			; no scroll bars
(unless (string-match "apple-darwin" system-configuration)
  ;; on mac, there's always a menu bar drown, don't have it empty
  (menu-bar-mode -1))

;; choose your own fonts, in a system dependant way
(if (string-match "apple-darwin" system-configuration)
    (set-face-font 'default "Monaco-13")
  (set-face-font 'default "Monospace-10"))

;;(global-hl-line-mode)			; highlight current line
(global-linum-mode 1)			; add line numbers on the left

;; avoid compiz manager rendering bugs
(add-to-list 'default-frame-alist '(alpha . 100))

;; copy/paste with C-c and C-v and C-x, check out C-RET too
(cua-mode)

;; under mac, have Command as Meta and keep Option for localized input
(when (string-match "apple-darwin" system-configuration)
  (setq mac-allow-anti-aliasing t)
  (setq mac-command-modifier 'meta)
  (setq mac-option-modifier 'none))

;; Use the clipboard, pretty please, so that copy/paste "works"
(setq x-select-enable-clipboard t)

;; Navigate windows with M-<arrows>
(windmove-default-keybindings 'meta)
(setq windmove-wrap-around t)

; winner-mode provides C-<left> to get back to previous window layout
(winner-mode 1)

;; whenever an external process changes a file underneath emacs, and there
;; was no unsaved changes in the corresponding buffer, just revert its
;; content to reflect what's on-disk.
(global-auto-revert-mode 1)

;; M-x shell is a nice shell interface to use, let's make it colorful.  If
;; you need a terminal emulator rather than just a shell, consider M-x term
;; instead.
(autoload 'ansi-color-for-comint-mode-on "ansi-color" nil t)
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

;; If you do use M-x term, you will notice there's line mode that acts like
;; emacs buffers, and there's the default char mode that will send your
;; input char-by-char, so that curses application see each of your key
;; strokes.
;;
;; The default way to toggle between them is C-c C-j and C-c C-k, let's
;; better use just one key to do the same.
(require 'term)
(define-key term-raw-map  (kbd "C-'") 'term-line-mode)
(define-key term-mode-map (kbd "C-'") 'term-char-mode)

;; Have C-y act as usual in term-mode, to avoid C-' C-y C-'
;; Well the real default would be C-c C-j C-y C-c C-k.
(define-key term-raw-map  (kbd "C-y") 'term-paste)

;; use ido for minibuffer completion
(require 'ido)
(ido-mode t)
(setq ido-save-directory-list-file "~/.emacs.d/.ido.last")
(setq ido-enable-flex-matching t)
;;(setq ido-use-filename-at-point 'guess)
;;(setq ido-show-dot-for-dired t)
(setq ido-file-extensions-order '(".org" ".txt" ".py" ".hs" ".v"))

;; default key to switch buffer is C-x b, but that's not easy enough
(global-set-key (kbd "C-b") 'ido-switch-buffer)

;; when you do that, to kill emacs either close its frame from the window
;; manager or do M-x kill-emacs.  Don't need a nice shortcut for a once a
;; week (or day) action.
;;(global-set-key (kbd "C-x C-c") 'ido-switch-buffer)
;;(global-set-key (kbd "C-x B") 'ibuffer)

;; C-x C-j opens dired with the cursor right on the file you're editing
(require 'dired-x)

;; full screen
(defun fullscreen ()
  (interactive)
  (set-frame-parameter nil 'fullscreen
		       (if (frame-parameter nil 'fullscreen) nil 'fullboth)))
(global-set-key [f11] 'fullscreen)
(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(safe-local-variable-values (quote ((coq-prog-args "-emacs-U" "-R" "/home/wires/w/corn" "CoRN" "-R" "/home/wires/w/corn/math-classes/src" "MathClasses") (coq-prog-args "-emacs-U" "-R" "." "CoRN" "-R" "math-classes/src" "MathClasses") (coq-prog-name . "/home/wires/opt/coq/coq-8.4beta/bin/coqtop") (coq-prog-args "-emacs-U")))))

;; processing-mode
(add-to-list 'load-path "/home/wires/opt/processing/processing-emacs/")
(autoload 'processing-mode "processing-mode" "Processing mode" t)
(add-to-list 'auto-mode-alist '("\\.pde$" . processing-mode))
(setq processing-location "/home/wires/opt/processing/processing-1.5.1/")

;; mmm mode for literate haskell
;;(add-hook 'haskell-mode-hook 'my-mmm-mode)

;; (require 'mmm-auto)

;; (mmm-add-classes
;;  '((literate-haskell
;;     :classes (literate-haskell-bird
;; 	      literate-haskell-laTeX)
;;     )
;;    (literate-haskell-bird
;;     :submode literate-haskell-mode
;;     :front "^>"
;;     :include-front t
;;     :back "^[^>]\\|\\'"
;;     :include-back nil
;;     :insert ((?b insert-literate-haskell-bird-region
;; 		 nil
;; 		 @ ">" @ " " _ @ "\n" @ "\n")))
;;    (literate-haskell-laTeX
;;     :submode haskell-mode
;;     :front "^\\\\begin{code}\n"
;;     :include-front nil
;;     :back "^\\\\end{code}"
;;     :include-back nil
;;     :insert ((?l insert-literate-haskell-laTeX-region
;; 		 nil
;; 		 @ "\\begin{code}\n" @ _ @ "\\end{code}" @ "\n")))
;;    (literate-haskell-lhs2TeX
;;     :classes (literate-haskell-lhs2TeX-code
;; 	      literate-haskell-lhs2TeX-bird-code
;; 	      literate-haskell-lhs2TeX-bird-spec
;; 	      ;; The following two modes cause mmm-parse-buffer to go
;; 	      ;; into a loop for || and @@.
;; 	      literate-haskell-lhs2TeX-verb
;; 	      literate-haskell-lhs2TeX-inline
;; 	      )
;;     )
;;    (literate-haskell-lhs2TeX-code
;;     :submode haskell-mode
;;     :front "^\\\\begin{code}\\|^\\\\begin{spec}"
;;     :front-offset (end-of-line 1)
;;     :back "^\\\\end{code}\\|^\\\\end{spec}"
;;     :back-offset (beginning-of-line -1)
;;     )
;;    (literate-haskell-lhs2TeX-bird-code
;;     :submode literate-haskell-mode
;;     :front "^> "
;;     :include-front true
;;     :back "^[^>]"
;;     :back-offset (beginning-of-line -1)
;;     )
;;    ;; literate-haskell-mode doesn't understand <...
;;    ;; mmm-mode doesn't handle :back "$" :back-offset 0 very well. The
;;    ;; keyboard bindings of the two modes aren't handled correctly.
;;    (literate-haskell-lhs2TeX-bird-spec
;;     :submode haskell-mode
;;     :front "^< "
;;     :back "^[^>]"
;;     :back-offset -1
;;     )
;;    (literate-haskell-lhs2TeX-verb
;;     :submode haskell-mode
;;     :front "@"
;;     :back "@"
;;     :back-offset -1
;;     )
;;    (literate-haskell-lhs2TeX-inline
;;     :submode haskell-mode
;;     :front "|"
;;     :back "\|"
;; ;;    :back-offset -1
;;     )
;; ))


;; ;;(defun my-mmm-mode ()
;;   ;; go into mmm minor mode when class is given
;;   ;;(make-local-variable 'mmm-global-mode)
;;   ;;(setq mmm-global-mode 'true))

;; ;;'(mmm-add-mode-ext-class LaTeX-mode "\\.lhs$" literate-haskell-lhs2TeX)

;; ;;'(mmm-global-mode t)
;; '(mmm-submode-decoration-level 1)

;; ;;(add-to-list 'auto-mode-alist '("\\.lhs$" . latex-mode))
;; (add-to-list 'mmm-mode-ext-classes-alist
;;              '(latex-mode "\\.lhs$" literate-haskell-lhs2TeX))

;; ;;'(mmm-mode-ext-classes-alist (quote ((latex-mode "\\.lhs$" literate-haskell-lhs2TeX) (text-mode "\\.lhs$" literate-haskell))) nil (mmm-mode))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 )
